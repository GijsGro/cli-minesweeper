# Minesweeper Game

This is a command-line Minesweeper game implemented in Java. The objective of the game is to uncover all the cells on the board that do not contain mines without detonating any mines.

## Getting Started

To run the Minesweeper game, follow these steps:

1. Make sure you have Java installed on your machine.
2. Clone the repository or download the source code.
3. Compile the Java source files using the Java compiler: `javac Minesweeper.java`.
4. Run the game using the Java Virtual Machine: `java Minesweeper`.

## How to Play

1. The game board will be displayed in the console.
2. Enter the coordinates of the cell you want to uncover in the format `x.y`, `x-y` or `x+y`. For example, to uncover the cell at row 2, column 3, enter `2+3`.
3. If the cell contains a mine, you lose the game. If it does not contain a mine, the cell will be uncovered and display the number of adjacent mines.
4. Continue uncovering cells until you have uncovered all non-mine cells or hit a mine.
5. If you uncover all non-mine cells, you win the game!

## Customization

You can customize the game by modifying the following parameters in the `play` method of the `Minesweeper.java` file:

- `height`: The height of the game board (number of rows).
- `width`: The width of the game board (number of columns).
- `minecount`: The number of mines to be placed on the board.

## License

This Minesweeper game is licensed under the MIT License. Feel free to use and modify the code for your own projects.
