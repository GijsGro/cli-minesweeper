/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package qquest.minesweeper;

import java.util.Arrays;
import java.util.Random;
//import javax.swing.JOptionPane;
import java.util.Scanner;

/**
 *
 * @author Gijs Groeneveld
 */

public class Minesweeper {

    public static void main(String[] args) {
        play();
    }

    public static int[][] setup(int width, int height, int minecount) {
        /**
         * Generates a 2D array representing the game board for the Minesweeper game, using -1 to indicate a mine
         *
         * @param  width     the width of the game board
         * @param  height    the height of the game board
         * @param  minecount the number of mines to be placed on the game board
         * @return           a 2D array representing mines (-1) on the board, and the number of adjacent mines for other cells
         */
        int[][] adjacent = new int[height][width];
        Random rand = new Random();
        int x;
        int y;
        int n = 0;
        if (minecount > width * height) {
            for (int i=0;i<width;i++) { for (int j=0;j<height;j++) { adjacent[i][j]=-1; } }
        } else {
            while (n < minecount) {
                x = rand.nextInt(height);
                y = rand.nextInt(width);
                if (adjacent[x][y] != -1) {
                    n = n + 1;
                    adjacent[x][y] = -1;
                }
            }
            bombCount(height, width, adjacent);
        }
        System.out.println(adjacent.length + " bij " + adjacent[0].length);
        return adjacent;
    }

    private static void bombCount(int height, int width, int[][] adjacent) {
        /**
         * Counts the number of adjacent mines at each cell, and stores the value the 'adjacent' array
         *
         * @param  height   the height of the array
         * @param  width    the width of the array
         * @param  adjacent the two-dimensional array representing the game board
         */
        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                if (adjacent[x][y] != -1) {
                    adjacent[x][y] = hasBomb(adjacent,x-1,y-1) +
                            hasBomb(adjacent,x-1,y) +
                            hasBomb(adjacent,x-1,y+1) +
                            hasBomb(adjacent,x,y-1) +
                            hasBomb(adjacent,x,y+1) +
                            hasBomb(adjacent,x+1,y-1)+
                            hasBomb(adjacent,x+1,y) +
                            hasBomb(adjacent,x+1,y+1);
                }
            }
        }
    }
    
    private static int hasBomb(int[][] adj, int x, int y) {
        /**
         * Determines if a bomb is present at the given (x,y)-coordinates
         *
         * @param  adj   a 2D array representing the adjacency values of cells (-1 if a mine)
         * @param  x     the x-coordinate of the location to check
         * @param  y     the y-coordinate of the location to check
         * @return       1 if a bomb is present at the given coordinates, 0 otherwise
         */
        if (x < 0 || y < 0 || x >=adj.length || y >= adj[0].length) {
            return 0;
        }
        if (adj[x][y] == -1) {
            return 1;
        } else {
            return 0;
        }
    }

    public static void play() {
        /**
         * Initializes the game board and runs until the game is won
         */
        int[][] adj = setup(7,9, 8);
            try (Scanner userin = new Scanner(System.in)) {
            String input;
            boolean[][] show = new boolean[adj.length][adj[0].length];
            printboard(show,adj);
            while (true) {
                System.out.println("Open:");
                input = userin.nextLine();
                if (input == null || input.isEmpty()) continue;
                if (testInput(input, adj, show)) break;
            }
        }
    }

    private static boolean testInput(String input, int[][] adj, boolean[][] show) {
        /**
         * Checks the validity of the input, and if so it opens the given cell
         *
         * @param  input  the input string to be checked and processed
         * @param  adj     a 2D array representing the adjacency values of cells (-1 if a mine)
         * @param  show    a boolean 2D array representing the visibility of cells
         * @return        true if the game is won, false otherwise
         */
        int x;
        int y;
        String[] parts = input.split("[.+-]");
        if (parts.length >= 2) {
            x = Integer.parseInt(parts[0]) - 1;
            y = Integer.parseInt(parts[1]) - 1;
            if (x < 0 || x >= adj.length || y < 0 || y >= adj[0].length) {
                System.out.println("Input out of bounds!");
            } else if (adj[x][y] == -1) {
                open(show, adj, x, y);
                printboard(show, adj);
                System.out.println("You lose!");
                return true;
            } else {
                open(show, adj, x, y);
                printboard(show, adj);
            }
            if (win(show, adj)) {
                System.out.println("You win!");
                return true;
            }
        }
        return false;
    }
    
    public static void open(boolean[][] show, int[][] adj, int x, int y) {
        /**
         * Recursively opens cells and, if 0-valued, also opens adjacent cells
         *
         * @param  show    a boolean 2D array representing the visibility of cells
         * @param  adj     a 2D array representing the adjacency values of cells (-1 if a mine)
         * @param  x      the x-coordinate of the cell to open
         * @param  y      the y-coordinate of the cell to open
         */
        int height = adj.length;
        int width = adj[0].length;
        if (x >= 0 && x < height && y >= 0 && y < width) {
            if (!show[x][y]) {
                show[x][y] = true;
                if (adj[x][y] == 0) {
                    open(show,adj,x-1,y-1);
                    open(show,adj,x  ,y-1);
                    open(show,adj,x+1,y-1);
                    open(show,adj,x-1,y  );
                    open(show,adj,x+1,y  );
                    open(show,adj,x-1,y+1);
                    open(show,adj,x  ,y+1);
                    open(show,adj,x+1,y+1);
                }
            }
        }
    }

    public static void printboard(boolean[][] show, int[][] adj) {
        /**
         * Prints the game board.
         *
         * @param  show    a boolean 2D array representing the visibility of cells
         * @param  adj     a 2D array representing the adjacency values of cells (-1 if a mine)
         */
        System.out.print("x y");
        for (int j = 0; j < adj[0].length; j++) {
            System.out.print("  " + (j + 1) + " ");
        }
        System.out.println();
        System.out.print("---");
        for (int j = 0; j < adj[0].length; j++) {
            System.out.print("----");
        }
        System.out.println("-");
        for (int i = 0; i < adj.length; i++) {
            System.out.print((i + 1) + " |");
            for (int j = 0; j < adj[0].length; j++) {
                if (show[i][j]) {
                    if (adj[i][j] > 0) {
                        System.out.print("| " + adj[i][j] + " ");
                    } else if (adj[i][j] == -1) {
                        System.out.print("| * ");
                    } else {
                        System.out.print("| . ");
                    }
                } else {
                    System.out.print("|   ");
                }
            }
            System.out.println("|");
        }
        System.out.println();
    }
    
    public static boolean win(boolean[][] show, int[][] adj) {
        /**
         * Determines whether the win-condition has been met
         *
         * @param  show    a boolean 2D array representing the visibility of cells
         * @param  adj     a 2D array representing the adjacency values of cells (-1 if a mine)
         * @return       true if the win-condition has been met, false otherwise
         */
        boolean win = false;
        for (int i = 0; i < adj.length; i++) {
            for (int j = 0; j < adj[0].length; j++) {
                if (adj[i][j] >= 0) {
                    win = (win || !show[i][j]);
                }
            }
        }
        return !win;
    }

}
